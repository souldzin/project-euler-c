#include <stdio.h>

int main() {
  printf("\n------------------------------\n");
  printf("PROJECT EULER PROBLEM 1");
  printf("\n------------------------------\n");
  printf("\n");

  unsigned int max = 1000;
  unsigned int sum = 0;

  for (int i = 1; i <= max; i++) {
    if (i % 3 == 0 || i % 5 == 0) {
      sum += i;
    }
  }

  printf("Answer: %u\n\n", sum);

  return 0;
}
