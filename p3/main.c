#include <math.h>
#include <stdio.h>

/**

<p>The prime factors of $13195$ are $5, 7, 13$ and $29$.</p>
<p>What is the largest prime factor of the number $600851475143$?</p>

 */

unsigned long max(const unsigned long a, const unsigned long b) {
  return a > b ? a : b;
}

unsigned long findLargestPrimeFactor(unsigned long long num) {
  unsigned long limit = sqrt(num);
  unsigned long maxFactor = 1;

  for (unsigned long i = limit; i > maxFactor; i--) {
    if (num % i == 0) {
      maxFactor = max(maxFactor, findLargestPrimeFactor(i));
    }
  }

  return maxFactor == 1 ? num : maxFactor;
}

int main() {
  printf("\n------------------------------\n");
  printf("PROJECT EULER PROBLEM 3");
  printf("\n------------------------------\n");
  printf("\n");

  unsigned long long num = 600851475143;
  unsigned long pf = findLargestPrimeFactor(num);

  printf("Answer: %lu\n\n", pf);

  return 0;
}
