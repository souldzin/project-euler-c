#include "../utils/index.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/**

<p>$2520$ is the smallest number that can be divided by each of the numbers from
$1$ to $10$ without any remainder.</p> <p>What is the smallest positive number
that is <strong class="tooltip">evenly divisible<span
class="tooltiptext">divisible with no remainder</span></strong> by all of the
numbers from $1$ to $20$?</p>

 */

bool isRelativePrime(uint64_t num, peutils_Collection *primes) {
  peutils_Iterator *primesIterator = peutils_createIterator(primes);
  bool result = 1;
  // test

  for (peutils_IteratorResult current = peutils_next(primesIterator);
       !current.done; current = peutils_next(primesIterator)) {
    uint64_t val = (uint64_t)(uintptr_t)current.value;

    if (num % val == 0) {
      result = false;
      break;
    }
  }

  peutils_destroy(primesIterator);
  return result;
}

peutils_Collection *getAllPrimesLessThan(uint64_t max) {
  peutils_Collection *primes = peutils_collection_create(LinkedList, {});

  if (max < 2) {
    return primes;
  }

  peutils_append(primes, (void *)(uintptr_t)2);

  for (uint64_t i = 3; i <= max; i += 2) {
    if (isRelativePrime(i, primes)) {
      void *iPtr = (void *)(uintptr_t)i;
      peutils_append(primes, iPtr);
    }
  }

  return primes;
}

uint64_t getSmallestNumberDivisibleByAll(uint64_t max) {
  peutils_Collection *primes = getAllPrimesLessThan(max);
  peutils_Iterator *primesIterator = peutils_createIterator(primes);

  uint64_t product = 1;
  for (peutils_IteratorResult current = peutils_next(primesIterator);
       !current.done; current = peutils_next(primesIterator)) {
    uint64_t factor = (uint64_t)(uintptr_t)current.value;
    printf("[product *= factor] product=%lu, factor=%lu\n", product,
           (uint64_t)(intptr_t)current.value);
    product *= factor;
  }

  peutils_destroy(primes);

  for (uint64_t i = 2; i < max; i++) {
    peutils_Collection *primeFactors = peutils_getPrimeFactors(i);
    printf("PrimeFactors of %lu are [", i);

    peutils_Iterator *primeFactorsIterator =
        peutils_createIterator(primeFactors);
    for (peutils_IteratorResult current = peutils_next(primeFactorsIterator);
         !current.done; current = peutils_next(primeFactorsIterator)) {
      printf("%lu, ", (uint64_t)(intptr_t)current.value);
    }

    printf("]\n");
    peutils_destroy(primeFactorsIterator);
    peutils_destroy(primeFactors);
  }

  return product;
}

int main() {
  printf("\n------------------------------\n");
  printf("PROJECT EULER PROBLEM 5");
  printf("\n------------------------------\n");
  printf("\n");

  uint64_t answer = getSmallestNumberDivisibleByAll(5);

  printf("Answer: %lu\n\n", answer);

  return 0;
}
