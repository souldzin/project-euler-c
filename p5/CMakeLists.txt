cmake_minimum_required(VERSION 3.16)

project(project_euler_5)

add_executable(project_euler_5 main.c)

add_subdirectory("${PROJECT_SOURCE_DIR}/../utils" "${PROJECT_SOURCE_DIR}/../utils/build")

target_link_libraries(project_euler_5 PUBLIC project_euler_utils)
target_include_directories(project_euler_5 PUBLIC
	"${PROJECT_BINARY_DIR}"
	"${PROJECT_SOURCE_DIR}/../utils"
	)

