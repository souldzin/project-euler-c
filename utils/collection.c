#include "collection.h"

void peutils_append(peutils_Collection* this, void* data) {
	this->vtable->append(this, data);
}

peutils_Iterator* peutils_createIterator(peutils_Collection* this) {
	return this->vtable->createIterator(this);
}

void peutils_collection_destroy(peutils_Collection* this) {
	this->vtable->destroy(this);
}
