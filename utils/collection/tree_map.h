#ifndef UTILS_COLLECTION_TREE_MAP_H
#define UTILS_COLLECTION_TREE_MAP_H

#include <assert.h>
#include <stdlib.h>

#define DECLARE_TREE_MAP(Type, key_type, value_type)                           \
  typedef struct Type##GetResult {                                             \
    bool exists;                                                               \
    value_type value;                                                          \
  } Type##GetResult;                                                           \
                                                                               \
  typedef struct Type##TreeNode {                                              \
    key_type key;                                                              \
    value_type value;                                                          \
    struct Type##TreeNode *left;                                               \
    struct Type##TreeNode *right;                                              \
  } Type##TreeNode;                                                            \
                                                                               \
  typedef struct Tree##TreeNodeFindResult {                                    \
    Type##TreeNode *node;                                                      \
    Type##TreeNode *parent;                                                    \
  } Type##TreeNodeFindResult;                                                  \
                                                                               \
  void Type##TreeNode_free(Type##TreeNode *treeNode, bool inclSelf) {          \
    if (!treeNode) {                                                           \
      return;                                                                  \
    }                                                                          \
                                                                               \
    Type##TreeNode_free(treeNode->left, true);                                 \
    Type##TreeNode_free(treeNode->right, true);                                \
                                                                               \
    if (inclSelf) {                                                            \
      free(treeNode);                                                          \
    }                                                                          \
  }                                                                            \
                                                                               \
  typedef int (*Type##KeyCompare)(const key_type, const key_type);             \
                                                                               \
  typedef struct Type {                                                        \
    size_t len;                                                                \
    size_t ldepth;                                                             \
    size_t rdepth;                                                             \
    Type##KeyCompare compareKeys;                                              \
    Type##TreeNode *root;                                                      \
  } Type;                                                                      \
                                                                               \
  Type Type##_new(Type##KeyCompare compareKeys) {                              \
    assert(compareKeys != NULL);                                               \
                                                                               \
    Type tree = {                                                              \
        .len = 0,                                                              \
        .ldepth = 0,                                                           \
        .rdepth = 0,                                                           \
        .compareKeys = compareKeys,                                            \
        .root = NULL,                                                          \
    };                                                                         \
    return tree;                                                               \
  }                                                                            \
                                                                               \
  void Type##_free(Type *tree, bool inclSelf) {                                \
    if (!tree) {                                                               \
      return;                                                                  \
    }                                                                          \
                                                                               \
    Type##TreeNode_free(tree->root, true);                                     \
                                                                               \
    if (inclSelf) {                                                            \
      free(tree);                                                              \
    }                                                                          \
  }                                                                            \
                                                                               \
  size_t Type##_length(Type *tree) { return tree ? tree->len : 0; }            \
                                                                               \
  Type##TreeNodeFindResult Type##TreeNode_find(                                \
      Type##TreeNode *treeNode, Type##TreeNode *parent, key_type key,          \
      Type##KeyCompare compareKeys) {                                          \
                                                                               \
    if (!treeNode) {                                                           \
      Type##TreeNodeFindResult result = {                                      \
          .node = NULL,                                                        \
          .parent = parent,                                                    \
      };                                                                       \
      return result;                                                           \
    }                                                                          \
                                                                               \
    int cmp = compareKeys(treeNode->key, key);                                 \
                                                                               \
    if (cmp == 0) {                                                            \
      Type##TreeNodeFindResult result = {                                      \
          .node = treeNode,                                                    \
          .parent = parent,                                                    \
      };                                                                       \
      return result;                                                           \
    }                                                                          \
                                                                               \
    if (cmp < 0) {                                                             \
      return Type##TreeNode_find(treeNode->left, treeNode, key, compareKeys);  \
    }                                                                          \
                                                                               \
    return Type##TreeNode_find(treeNode->right, treeNode, key, compareKeys);   \
  }                                                                            \
                                                                               \
  Type##GetResult Type##_get(Type *tree, key_type key) {                       \
    if (!tree) {                                                               \
      Type##GetResult x = {.exists = false};                                   \
      return x;                                                                \
    }                                                                          \
                                                                               \
    Type##TreeNodeFindResult findResult =                                      \
        Type##TreeNode_find(tree->root, NULL, key, tree->compareKeys);         \
                                                                               \
    Type##TreeNode *treeNode = findResult.node;                                \
                                                                               \
    if (!treeNode) {                                                           \
      Type##GetResult x = {.exists = false};                                   \
      return x;                                                                \
    }                                                                          \
                                                                               \
    Type##GetResult x = {.exists = true, .value = treeNode->value};            \
    return x;                                                                  \
  }                                                                            \
                                                                               \
  void Type##_set(Type *tree, key_type key, value_type value) {                \
    if (!tree) {                                                               \
      return;                                                                  \
    }                                                                          \
                                                                               \
    Type##TreeNodeFindResult findResult =                                      \
        Type##TreeNode_find(tree->root, NULL, key, tree->compareKeys);         \
                                                                               \
    if (findResult.node) {                                                     \
      findResult.node->value = value;                                          \
      return;                                                                  \
    }                                                                          \
                                                                               \
    Type##TreeNode *node = malloc(sizeof(Type##TreeNode));                     \
    node->key = key;                                                           \
    node->value = value;                                                       \
    node->left = NULL;                                                         \
    node->right = NULL;                                                        \
                                                                               \
    if (!findResult.parent) {                                                  \
      tree->root = node;                                                       \
      tree->len = 1;                                                           \
      return;                                                                  \
    }                                                                          \
                                                                               \
    int cmp = tree->compareKeys(findResult.parent->key, key);                  \
                                                                               \
    if (cmp < 0) {                                                             \
      assert(!findResult.parent->left);                                        \
      findResult.parent->left = node;                                          \
    } else {                                                                   \
      assert(!findResult.parent->right);                                       \
      findResult.parent->right = node;                                         \
    }                                                                          \
                                                                               \
    tree->len += 1;                                                            \
  }

#endif
