#ifndef PEUTILS_ITERATOR_H
#define PEUTILS_ITERATOR_H 1
#include <stdbool.h>

typedef struct peutils_IteratorResult {
	void* value;
	bool done;
} peutils_IteratorResult;

typedef struct peutils_Iterator {
	const struct peutils_IteratorInterface* const vtable;
} peutils_Iterator;

typedef struct peutils_IteratorInterface  {
	const peutils_IteratorResult (*next)(peutils_Iterator*);
	void (*destroy)(peutils_Iterator*);
} peutils_IteratorInterface;

const peutils_IteratorResult peutils_next(peutils_Iterator*);
void peutils_iterator_destroy(peutils_Iterator*);

#endif /* PEUTILS_ITERATOR_H */
