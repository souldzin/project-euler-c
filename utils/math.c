#include <math.h>
#include "collection.h"
#include "linked_list.h"
#include "math.h"

peutils_Collection* peutils_getPrimeFactors(uint64_t num) {
	peutils_Collection* list = peutils_collection_create(LinkedList, {});

	while (num > 1) {
		if (num % 2 == 0) {
			peutils_append(list, (void*)(uintptr_t)2);
			num = num / 2;
			continue;
		}

		uint64_t maxFactor = sqrt(num);
		uint64_t primeFactor = num;

		for (uint64_t i = 3; i <= maxFactor; i += 2) {
			if (num % i == 0) {
				primeFactor = i;
				break;
			}
		}

		peutils_append(list, (void*)(uintptr_t)primeFactor);
		num = num / primeFactor;
	}

	return list;
}
