#ifndef PEUTILS_ALIAS_H
#define PEUTILS_ALIAS_H 1

#define PEUTILS_ALIAS(X) typedef peutils_##X X

#define peutils_destroy(X) \
	_Generic((X), \
		peutils_Collection*: peutils_collection_destroy, \
		peutils_Iterator*: peutils_iterator_destroy \
	)(X)

#endif /* PEUTILS_ALIAS_H */
