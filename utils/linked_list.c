#include <stdlib.h>
#include <string.h>
#include "linked_list.h"
#include "alias.h"

PEUTILS_ALIAS(Collection);
PEUTILS_ALIAS(CollectionInterface);
PEUTILS_ALIAS(LinkedList);
PEUTILS_ALIAS(LinkedListNode);
PEUTILS_ALIAS(LinkedListOptions);
PEUTILS_ALIAS(Iterator);
PEUTILS_ALIAS(IteratorInterface);
PEUTILS_ALIAS(IteratorResult);

// region: LinkedListIterator --------------
typedef struct LinkedListIterator {
	Iterator* base;
	LinkedList* _list;
	LinkedListNode* _current;
	bool _done;
} LinkedListIterator;

const IteratorResult LinkedListIterator_next(Iterator* base) {
	LinkedListIterator* this = (LinkedListIterator*) base;

	if (this->_done) {
		return (IteratorResult) {
			.done = true,
			.value = NULL
		};
	}

	if (this->_current == NULL) {
		this->_current = this->_list->_head;
	} else {
		this->_current = this->_current->next;
	}

	if (this->_current == NULL) {
		this->_done = true;

		return (IteratorResult) {
			.done = true,
			.value = NULL
		};
	}

	return (IteratorResult) {
		.done = false,
		.value = this->_current->data,
	};
}

void LinkedListIterator_destroy(Iterator* base) {
	LinkedListIterator* this = (LinkedListIterator*) base;

	free(this);
}

LinkedListIterator* createLinkedListIterator(LinkedList* list) {
	// assign function implementations to interface
	static const IteratorInterface vtable = {
		LinkedListIterator_next,
		LinkedListIterator_destroy
	};
	static Iterator base = {&vtable};

	LinkedListIterator* this = malloc(sizeof(LinkedListIterator));
	memcpy(&this->base, &base, sizeof(base));
	this->_list = list;
	this->_current = NULL;
	this->_done = false;

	return this;
}

// region: LinkedList ----------------------

Iterator* createIterator(Collection* base) {
	LinkedList* this = (LinkedList*)base;

	LinkedListIterator* iterator = createLinkedListIterator(this);

	return (Iterator*)iterator;
}

void append(Collection* base, void* data) {
	LinkedList* list = (LinkedList*)base;
	
	LinkedListNode* newNode = malloc(sizeof(LinkedListNode));
	newNode->next = NULL;
	newNode->data = data;

	if (list->_head == NULL) {
		list->_head = newNode;
		return;
	}

	LinkedListNode* current = list->_head;

	while (current->next != NULL) {
		current = current->next;
	}

	current->next = newNode;
}

void destroy(Collection* base) {
	LinkedList* list = (LinkedList*)base;
	if (list == NULL) {
		return;
	}

	LinkedListNode* current = list->_head;
	LinkedListNode* next;

	while (current != NULL) {
		next = current->next;

		if (list->_destroyData != NULL) {
			list->_destroyData(current->data);
		}
		free(current);
		current = next;
	}

	free(list);
}

LinkedList* peutils_LinkedList_create(const LinkedListOptions options) {
	// assign function implementations to interface
	static const CollectionInterface vtable = {
		append,
		createIterator,
		destroy
	};
	static Collection base = {&vtable};

	LinkedList* list = malloc(sizeof(LinkedList));
	memcpy(&list->base, &base, sizeof(base));

	if (list == NULL) {
		return list;
	}

	list->_head = NULL;
	list->_destroyData = options.destroyData;

	return list;
}
