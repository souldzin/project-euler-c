#ifndef PEUTILS_MATH_H
#define PEUTILS_MATH_H 1

#include <stdint.h>
#include "collection.h"

peutils_Collection* peutils_getPrimeFactors(uint64_t num);

#endif
