#ifndef PEUTILS_LINKED_LIST_H
#define PEUTILS_LINKED_LIST_H 1

#include "collection.h"
#include "iterator.h"

typedef struct peutils_LinkedListNode {
	void* data;
	struct peutils_LinkedListNode* next;
} peutils_LinkedListNode;

typedef struct peutils_LinkedList {
	peutils_Collection* base;
	peutils_LinkedListNode* _head; 
	void (*_destroyData)(void* data);
} peutils_LinkedList;

typedef struct peutils_LinkedListOptions {
	void (*destroyData)(void*);
} peutils_LinkedListOptions;

peutils_LinkedList* peutils_LinkedList_create(const peutils_LinkedListOptions);

#endif /* PEUTILS_LINKED_LIST_H */
