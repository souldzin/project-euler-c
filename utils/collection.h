#ifndef PEUTILS_COLLECTION_H
#define PEUTILS_COLLECTION_H 1

#include "iterator.h"

typedef struct peutils_Collection {
	const struct peutils_CollectionInterface* const vtable;
} peutils_Collection;

typedef struct peutils_CollectionInterface {
  void (*append)(peutils_Collection*, void*);
	peutils_Iterator* (*createIterator)(peutils_Collection*);
	void (*destroy)(peutils_Collection*);
} peutils_CollectionInterface;

void peutils_append(peutils_Collection*, void*);
peutils_Iterator* peutils_createIterator(peutils_Collection*);
void peutils_collection_destroy(peutils_Collection*);

#define peutils_collection_create(X, Y) (\
		(peutils_Collection*) peutils_##X##_create((peutils_##X##Options) Y)\
	)

#endif /* PEUTILS_COLLECTION_H */
