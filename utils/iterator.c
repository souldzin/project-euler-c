#include "iterator.h"

const peutils_IteratorResult peutils_next(peutils_Iterator* this) {
	return this->vtable->next(this);
}

void peutils_iterator_destroy(peutils_Iterator* this) {
	this->vtable->destroy(this);
}
