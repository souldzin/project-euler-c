#!/bin/bash

ROOT_DIR=$(realpath $(dirname "$0")/..)
PROBLEM_NUM="$1"
NEW_DIR="p${PROBLEM_NUM}"
NEW_DIR_A="${ROOT_DIR}/${NEW_DIR}"

if [ -z "$PROBLEM_NUM" ]; then
	echo ""
	echo "ERROR: No problem number provided. Please provide a problem number for this script."
	echo "EXAMPLE: ./scripts/new_problem.sh 6"
	echo ""
	exit 1
fi

if ! [[ "$PROBLEM_NUM" =~ ^[1-9][0-9]*$ ]]; then
	echo ""
	echo "ERROR: Value for problem number is not a number (found: '${PROBLEM_NUM}'). Please provide a number containing only digits."
	echo "EXAMPLE: ./scripts/new_problem.sh 6"
	echo ""
	exit 1
fi

if [ -e "${NEW_DIR_A}" ]; then
	echo ""
	echo "ERROR: './${NEW_DIR}' already exists. Please try a different problem number or remove './${NEW_DIR}' manually."
	echo ""
	exit 1
fi

set -e

mkdir ${NEW_DIR_A}
wget -O "${NEW_DIR_A}/problem.html" "https://projecteuler.net/minimal=${PROBLEM_NUM}"

cat <<EOF > ${NEW_DIR_A}/CMakeLists.txt
cmake_minimum_required(VERSION 3.16)

project(project_euler_${PROBLEM_NUM})

add_executable(project_euler_${PROBLEM_NUM} main.c)
EOF

cat <<EOF > ${NEW_DIR_A}/main.c
#include <stdio.h>

/**

$(cat ${NEW_DIR_A}/problem.html)

 */

int main() {
	printf("\n------------------------------\n");
	printf("PROJECT EULER PROBLEM ${PROBLEM_NUM}");
	printf("\n------------------------------\n");
	printf("\n");

	unsigned int sum = 0;

	printf("Answer: %d\n\n", sum);

	return 0;
}
EOF

mkdir ${NEW_DIR_A}/build
cd ${NEW_DIR_A}/build
cmake ..

echo ""
echo "Scaffolding complete! Project Euler problem ${PROBLEM_NUM} is ready at './${NEW_DIR}'."
echo ""
