ROOT_DIR=$(realpath $(dirname "$0")/..)
TEST_DIR="$ROOT_DIR/tests"

set -e

if [[ "$1" == "-f" ]]; then
  rm -rf "${TEST_DIR}/build"
fi

if ! [[ -d "${TEST_DIR}/build" ]]; then
	mkdir "${TEST_DIR}/build"
	cd "${TEST_DIR}/build"
	cmake ..
fi

cd ${TEST_DIR}/build
make -s
ctest --progress --output-on-failure
