#!/bin/bash

ROOT_DIR=$(realpath $(dirname "$0")/..)
PROBLEM_NUM="$1"
PROBLEM_DIR="p${PROBLEM_NUM}"
PROBLEM_DIR_A="${ROOT_DIR}/${PROBLEM_DIR}"

if [ -z "$PROBLEM_NUM" ]; then
	echo ""
	echo "ERROR: No problem number provided. Please provide a problem number for this script."
	echo "EXAMPLE: ./scripts/run_problem.sh 1"
	echo ""
	exit 1
fi

if ! [[ "$PROBLEM_NUM" =~ ^[1-9][0-9]*$ ]]; then
	echo ""
	echo "ERROR: Value for problem number is not a number (found: '${PROBLEM_NUM}'). Please provide a number containing only digits."
	echo "EXAMPLE: ./scripts/run_problem.sh 1"
	echo ""
	exit 1
fi

if ! [[ -d "${PROBLEM_DIR_A}" ]]; then
	echo ""
	echo "ERROR: './${PROBLEM_DIR}' does not exists. Please try a different problem number to run."
	echo "Do you mean to run './scripts/new_problem.sh ${PROBLEM_NUM}'?"
	echo ""
	exit 1
fi

set -e

if ! [[ -d "${PROBLEM_DIR_A}/build" ]]; then
	mkdir "${PROBLEM_DIR_A}/build"
	cd "${PROBLEM_DIR_A}/build"
	cmake ..
fi

cd ${PROBLEM_DIR_A}/build
make -s
./project_euler_${PROBLEM_NUM}
