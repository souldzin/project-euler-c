#include "collection/tree_map.h"
#include <string.h>
#include <unity.h>

DECLARE_TREE_MAP(TestTreeMap, char *, int);

void setUp() {}

void tearDown() {}

void test_TreeMap_whenEmpty(void) {
  TestTreeMap x = TestTreeMap_new(strcmp);

  TEST_ASSERT_EQUAL(0, TestTreeMap_length(&x));
  TEST_ASSERT_EQUAL(false, TestTreeMap_get(&x, "").exists);

  TestTreeMap_free(&x, false);
}

void test_TreeMap_withItems(void) {
  TestTreeMap x = TestTreeMap_new(strcmp);

  TestTreeMap_set(&x, "a", 1);
  TestTreeMap_set(&x, "b", 7);
  TestTreeMap_set(&x, "a", 5);

  TEST_ASSERT_EQUAL(2, TestTreeMap_length(&x));
  TEST_ASSERT_EQUAL(true, TestTreeMap_get(&x, "a").exists);
  TEST_ASSERT_EQUAL(5, TestTreeMap_get(&x, "a").value);

  TestTreeMap_free(&x, false);
}

int main(void) {
  UNITY_BEGIN();

  RUN_TEST(test_TreeMap_whenEmpty);
  RUN_TEST(test_TreeMap_withItems);

  return UNITY_END();
}
