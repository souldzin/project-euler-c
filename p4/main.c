#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**

<p>A palindromic number reads the same both ways. The largest palindrome made
from the product of two $2$-digit numbers is $9009 = 91 \times 99$.</p> <p>Find
the largest palindrome made from the product of two $3$-digit numbers.</p>

 */

char *allocAsString(uint64_t num) {
  size_t maxDigits = 20;
  char *buffer = malloc((maxDigits + 1) * sizeof(char));

  if (buffer == NULL) {
    free(buffer);
    printf("ERROR [allocAsString]: Failed to allocate memory.");
    return NULL;
  }

  int result = snprintf(buffer, maxDigits, "%lu", num);

  if (result < 0) {
    free(buffer);
    printf("ERROR [allocAsString]: Failed to write number.");
    return NULL;
  }

  return buffer;
}

bool isPalindrome(uint64_t num) {
  char *str = allocAsString(num);

  // printf("[DEBUG isPalindrome alloc str] %s\n", str);

  if (str == NULL) {
    return false;
  }

  size_t size = strlen(str);
  size_t halfSize = size / 2;
  bool result = true;

  for (size_t i = 0; i < halfSize; i++) {
    size_t j = size - 1 - i;
    // printf("[DEBUG isPalindrome] i=%d j=%d halfSize=%d size=%d\n", i, j,
    // halfSize, size);

    if (str[i] != str[j]) {
      result = false;
      break;
    }
  }

  free(str);
  return result;
}

uint64_t largestPalindromeProduct(uint64_t maxPart, uint64_t minPartInit) {
  uint64_t minPart = minPartInit;
  uint64_t maxProduct = 0;

  for (uint64_t i = maxPart; i >= minPart; i--) {
    for (uint64_t j = i; j >= minPart; j--) {
      uint64_t product = i * j;

      if (isPalindrome(product) && product > maxProduct) {
        maxProduct = product;
        minPart = j;
      }
    }
  }

  return maxProduct;
}

int main() {
  printf("\n------------------------------\n");
  printf("PROJECT EULER PROBLEM 4");
  printf("\n------------------------------\n");
  printf("\n");

  uint64_t answer = largestPalindromeProduct(999, 100);

  printf("Answer: %lu\n\n", answer);

  return 0;
}
